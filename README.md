### ZMESH - forked from [YADR](https://github.com/skwp/dotfiles.git)

* ZMESH has following changes from [YADR](https://github.com/skwp/dotfiles.git):
* `neovim` instead `vim`
* `vim-plug` instead `Vundle`

```bash
sh -c "`curl -fsSL https://bitbucket.org/codeaxe/zmesh/raw/35ed326512ba171a33ee84a98e31d69d44363b36/install.sh`"
```

* ZMESH will automatically install all of its subcomponents. If you want to be asked
about each one, use:

```bash
sh -c "`curl -fsSL https://bitbucket.org/codeaxe/zmesh/raw/35ed326512ba171a33ee84a98e31d69d44363b36/install.sh`" -s ask
```
### TODO: ###
* Add support for BASH
* Add more meaning full themes
